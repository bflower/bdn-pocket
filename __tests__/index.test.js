import assert from 'assert'
import {
  ActionType,
  Types,
  PropTypes,
  Action,
  Signal,
  Messenger,
  MakeReqMessenger,
  makeReducer,
  makePathReducer,
  Selector,
  SliceSelector,
  SelectorDef,
} from '../src/index'

import ActionTypeI from '../src/action_type'
import TypesI from '../src/types'
import PropTypesI from '../src/prop_types'
import ActionI from '../src/action'
import SignalI from '../src/signal'
import {
  Messenger as MessengerI,
  MakeReqMessenger as MakeReqMessengerI,
  makeReducer as makeReducerI,
  makePathReducer as makePathReducerI,
} from '../src/messenger'
import {
  Selector as SelectorI,
  SliceSelector as SliceSelectorI,
  SelectorDef as SelectorDefI,
} from '../src/selector'

test('index reference rights objects', () => {
  assert.equal(ActionType, ActionTypeI, 'action type')
  assert.equal(Types, TypesI, 'types')
  assert.equal(PropTypes, PropTypesI, 'prop types is ok')
  assert.equal(Action, ActionI, 'action')
  assert.equal(Signal, SignalI, 'signal')
  assert.equal(Messenger, MessengerI, 'messenger')
  assert.equal(MakeReqMessenger, MakeReqMessengerI, 'make req messenger')
  assert.equal(makeReducer, makeReducerI, 'make reducer')
  assert.equal(makePathReducer, makePathReducerI, 'make path reducer')
  assert.equal(Selector, SelectorI, 'selector')
  assert.equal(SliceSelector, SliceSelectorI, 'slice selector')
  assert.equal(SelectorDef, SelectorDefI, 'selector def')
})
