import assert from 'assert'
import is from '@stamp/is'
import Types, { Type, number, string } from '../src/types'

test('Types', () => {
  Object.keys(Types).forEach(
    ty => {
      assert.equal(
        is.isFunction(Types[ty]),
        true,
        `${ty} is a function`
      )
      assert.equal(
        is.isStamp(Types[ty]),
        true,
        `${ty} is a stamp`
      )
    }
  )
})

test('Types#isValid required', () => {
  const MyType = Type
    .props({ type: 'myType' })
    .methods({ isTypeOk() { return true }})

  expect(
    MyType().required,
    // 'by default, required is true'
  ).toBe(true)

  const myReqType = MyType({ required: true, name: 'a' })

  expect(
    myReqType.isValid({ a: 10 }),
    // 'an object is validated if required prop is present'
  ).toBe(true)

  expect(
    myReqType.isValid({ b: 10 }, 'b'),
    // 'an object is validated if required prop is present with propName param overriding internal name'
  ).toBe(true)

  assert.throws(
    () => { myReqType.isValid({b: ''}) },
    Error,
    'an object is not validated if required prop is not present'
  )

  const notReqtype = MyType({ required: false, name: 'a' })
  expect(
    notReqtype.isValid({ b: '' }),
    // 'an object is validated if not required prop is not present'
  ).toBe(true)
  expect(
    notReqtype.isValid({ a: '' }),
    // 'an object is validated if not required prop is present'
  ).toBe(true)
})

test('Types#isValid allowNull', () => {
  const MyType = Type
    .props({ type: 'myType' })
    .methods({ isTypeOk() { return true }})

  expect(
    MyType().allowNull,
    // 'by default allowNull is false'
  ).toBe(false)

  const myNullType = MyType({ allowNull: true, required: false, name: 'a' })

  expect(
    myNullType.isValid({ a: 10 }),
    // 'an object is validated if prop if prop val is set'
  ).toBe(true)

  expect(
    myNullType.isValid({ a: null }),
    // 'an object is validated if prop if prop val is set to null'
  ).toBe(true)
  expect(
    myNullType.isValid({ a: undefined }),
    // 'an object is validated if prop if prop val is set to undefined'
  ).toBe(true)

  const notNullType = MyType({ allowNull: false, required: false, name: 'a' })
  assert.throws(
    () => { notNullType.isValid({ a: null }) },
    Error,
    'an object is not validated if prop is null and allowNull is false'
  )
  expect(
    notNullType.isValid({ b: '' }),
    // 'object is validated if allowNull is false and prop not present'
  ).toBe(true)
})


test('Types#isValid with defined types', () => {
  const tOk = {
    number: { f: number, v: 1 },
    string: { f: string, v: 'a' },
  }
  const tNotOk = {
    number: { f: number, v: 'a' },
    string: { f: string, v: 1 },
  }

  Object.keys(tOk).forEach(
    type => {
      const s = tOk[type]
      expect(
        s.f().isValid({ a: s.v }, 'a'),
        // 'object is valid if prop has valid type'
      ).toBe(true)
    }
  )

  Object.keys(tNotOk).forEach(
    type => {
      const s = tNotOk[type]
      assert.throws(
        () => s.f().isValid({ a: s.v }),
        Error,
        'object is not valid if prop has not valid type'
      )
    }
  )
})
