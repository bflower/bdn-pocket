import assert from 'assert'
import R from 'ramda'
import { MakeReqMessenger, Messenger } from '../src/messenger'
import Action from '../src/action'
import { checkMessages } from './lib'

const DEF_MESSAGES = ['request', 'success', 'error', 'reset', 'ready']

test('ReqMessenger - create a messenger to handle request call', () => {
  const msgr = MakeReqMessenger({ name: 'hello' })
  checkMessages(msgr, DEF_MESSAGES)
})

test('ReqMessenger - overriding default reducers', () => {
  const myReqReducer = (state, action) => 'myReqReducer'
  const mySuccReducer = (state, action) => 'mySuccReducer'
  const MyReqMessenger = MakeReqMessenger
    .mergeReducers({
      request: myReqReducer,
      success: mySuccReducer
    })
  const msgr = MyReqMessenger({ name: 'hello' })
  assert.equal(
    msgr.reducers[msgr.request.CONST](),
    myReqReducer(),
    'defined request reducer is on place '
  )

  assert.equal(
    msgr.reducers[msgr.success.CONST](),
    mySuccReducer(),
    'defined success reducer is on place '
  )

  checkMessages(msgr, DEF_MESSAGES)
})

test('ReqMessenger - add your messages', () => {
  const msgr = MakeReqMessenger
    .add({
      key: 'a',
      action: Action.def('a'),
      reducer(state, action) { return state }
    })
    .create({ name: 'a'})

  checkMessages(msgr, DEF_MESSAGES.concat(['a']))
})
