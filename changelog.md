# bdn-pocket changelog

## V1.2

* New type "array" (see Types)
* Split behavior between `array` and `object`

**BREAKING CHANGES**

Because of the new `array` type, `object` type behavior changes and does not allow arrays anymore.

## v1.0 - v1.1

Initial versions
