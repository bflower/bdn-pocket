import R from 'ramda'
import assert from 'assert'

export function checkMessages(messenger, messageKeys) {
  messageKeys.forEach(key => {
    assert.ok(
      R.pathOr(false, [key, 'CONST'], messenger),
      'messenger has all messages - action part'
    )

    assert.ok(
      R.pathOr(false, ['reducers', messenger[key].CONST], messenger),
      'messenger has all messages - reducer part'
    )
  })
}

