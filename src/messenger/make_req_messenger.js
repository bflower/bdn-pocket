import merge from 'ramda/src/merge'
import union from 'ramda/src/union'
import omit from 'ramda/src/omit'
import propOr from 'ramda/src/propOr'

import Action from '../action'
import DefState from './def_state'
import Messenger from './messenger'


const REQ_MSG_REDUCERS = {
  request(state) {
    return merge(state, {
      isFetching: true,
      ready: false
    })
  },

  success(state, { payload }) {
    const { result, replace } = payload
    const xtra = omit(['result', 'entities', 'replace'], payload)

    const isArray = Array.isArray(state.result) && Array.isArray(result)
    const mergeResult = (!replace && isArray) ?
      union(state.result, result) :
      result

    return merge(state, {
      isFetching: false,
      ready: true,
      error: false,
      result: mergeResult,
      pageCount: (state.pageCount || 0) + 1,
      xtra,
    })
  },

  error(state) {
    return merge(state, {
      isFetching: false,
      ready: false,
      error: true,
    })
  },

  reset() {
    return this.defaultState
  },

  ready(state) {
    return merge(state, {
      isFetching: false,
      ready: true,
      error: false,
    })
  },
}

const MakeReqMessenger = DefState
  .defaultState({
    isFetching: false,
    ready: false,
    error: false,
    pageCount: 0,
    result: [],
  })
  .props({
    Action,
  })
  .deepProps({
    reqReducers: REQ_MSG_REDUCERS,
    reqActions: {},
  })
  .statics({
    Messenger,
    mergeReducers(reqReducers) {
      return this.deepProps({
        reqReducers,
      })
    },

    mergeActions(reqActions) {
      return this.deepProps({
        reqActions,
      })
    },

    add({ key, action, reducer }) {
      return this.statics({
        Messenger: this.Messenger.add({ key, action, reducer })
      })
    },

    setup(name) {
      const {
        Messenger,
      } = this.compose.staticProperties
      const {
        reqReducers,
        reqActions,
        defaultState,
      } = this.compose.deepProperties
      const {
        Action,
      } = this.compose.properties

      let ReqMessenger = Messenger.defaultState(defaultState)

      Object.keys(reqReducers).forEach(key => {
        const ActionKey = propOr(Action, key, reqActions)

        ReqMessenger = ReqMessenger.add({
          key,
          action: ActionKey.def(`${name}/${key}`),
          reducer: reqReducers[key],
        })
      })
      return ReqMessenger
    }
  })
  .init(({ name, schema }, { stamp }) => {
    const msgr = stamp.setup(name).create()
    msgr.schema = schema
    return msgr
  })

export default MakeReqMessenger
