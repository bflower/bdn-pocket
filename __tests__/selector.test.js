import R from 'ramda'
import assert from 'assert'
import is from '@stamp/is'
import Selector from '../src/selector/selector'


const state = {
  entities: {
    user: {
      me: {
        name: 'arnaud'
      }
    },
    comments: {
      one: {
        owner: 'me',
        text: 'hello',
      }
    }
  },
  users: {
    result: ['me']
  },
  comments: {
    me: {
      result: ['one']
    }
  }
}


test('Selector - simple select', () => {
  const comments = Selector
    .selectors({
      comPag: state => state.comments.me.result,
      comEnt: state => state.entities.comments,
    })
    .create({
      reducer({ comPag, comEnt }) {
        return comPag.map(
          comId => comEnt[comId]
        )
      }
    })

  assert.deepEqual(
    comments(state, { user: 'me' }),
    [state.entities.comments.one],
    'same as reselect.createStructuredSelector with more semantics'
  )
})

test('Selector - no props', () => {
  const comments = Selector
    .selectors({
      comPag: state => state.comments.me.result,
      comEnt: state => state.entities.comments,
    })
    .create({
      reducer({ comPag, comEnt }) {
        return comPag.map(
          comId => comEnt[comId]
        )
      }
    })

  const s = comments(state)
  expect(
    typeof s === 'function',
    // 'return a partial selector waiting for props (even empty)'
  ).toBe(true)

  assert.deepEqual(
    s({}),
    [state.entities.comments.one],
    'selector return selection with empty object as argument'
  )

  assert.deepEqual(
    s(),
    [state.entities.comments.one],
    'selector return selection with no argument'
  )
  assert.deepEqual(
    s({coucou: 'hello'}),
    [state.entities.comments.one],
    'selector return selection with object argument but not empty'
  )
})


test('Selector with props', () => {

  const comments = Selector
    .propTypes('user')
    .selectors({
      comPag: state => state.comments,
      comEnt: state => state.entities.comments,
    })
    .create({
      reducer({ comPag, comEnt }, { user }) {
        const userComments = comPag[user]
        return userComments.result.map(
          comId => comEnt[comId]
        )
      }
    })

  assert.deepEqual(
    comments(state, { user: 'me' }),
    [state.entities.comments.one],
    'allow reselection with props as arguments'
  )
})

test('Selector memoization', () => {

  let reducerCallCount = 0

  const comments = Selector
    .propTypes('user')
    .selectors({
      comPag: state => state.comments,
      comEnt: state => state.entities.comments,
    })
    .create({
      reducer({ comPag, comEnt }, { user }) {
        reducerCallCount++
        const userComments = comPag[user]
        return userComments ?
          userComments.result.map(
            comId => comEnt[comId]
          ) :
          []
      }
    })

  let res1 = comments(state, { user: 'me' }) // nb call: 1
  expect(
    res1 === comments(state, { user: 'me' }),
    // 'last call is memoized and return same ref with same state (ref comparison) & same props (deepEq comparison)'
  ).toBe(true)

  expect(
    res1 !== comments(state, { user: 'not me' }), // nb call: 2
    // 'new props value invalidate memoization'
  ).toBe(true)

  // restart memo
  res1 = comments(state, { user: 'me' }) // nb call: 3
  // change state outside of comments state shape selectors
  let newState = R.evolve({
    entities: {
      users: {
        me: R.merge(R.__, { name: 'paz'} )
      }
    }
  }, state)

  expect(
    res1 === comments(newState, { user: 'me' }), // nb call: 3
    // 'if sub state of selector remain unchanged will return same result as previous'
  ).toBe(true)

  // change sub state in commets state selectors
  newState = R.evolve({
    entities: {
      comments: {
        one: R.merge(R.__, { text: 'blabla'} )
      }
    }
  }, newState)
  expect(
    res1 !== comments(newState, { user: 'me' }), // nb call: 4
    // 'if sub state of selector change it will return different result'
  ).toBe(true)

  assert.equal(
    reducerCallCount,
    4,
    'reducer is called 4 times in this scenario'
  )
})


test('Selector partial', () => {
  const comments = Selector
    .propTypes('user')
    .selectors({
      comPag: R.path(['comments']),
      comEnt: R.path(['entities', 'comments']),
    })
    .create({
      reducer({ comPag, comEnt }, { user }) {
        return { res: 'my result' }
      }
    })


  const fn1 = comments(state)

  expect(
    is.isFunction(fn1),
    // 'Selector return a partial selector if props arg is missing'
  ).toBe(true)

  assert.equal(
    fn1({ user: 'me' }),
    comments(state, { user: 'me' }),
    'Partial selector use same memoization that full args selector'
  )

  assert.equal(
    fn1,
    comments(state),
    'Partial selector is memoized once - same state'
  )

  assert.notEqual(
    fn1,
    comments({}),
    'Partial selector is memoized once - state changed'
  )
})


test('Selector as subselector', () => {
  let comRedCalled = false
  let userRedCalled = false

  const comments = Selector
    .propTypes('user')
    .selectors({
      comEnt: R.path(['entities', 'comments']),
    })
    .create({
      reducer() {
        comRedCalled = true
        return ['comment', 'result']
      }
    })


  const user = Selector
    .propTypes('user')
    .selectors({
      userEnt: R.path(['entities', 'user']),
      commentSel: comments
    })
    .create({
      reducer({ commentSel }, { user }) {
        userRedCalled = true
        return {
          user: ['user', 'result'],
          comments: commentSel({ user }),
        }
      }
    })

  function compareCalled(pComRedCalled, pUserRedCalled ) {
    const c = { comRedCalled, userRedCalled }
    comRedCalled = false
    userRedCalled = false

    return R.equals(
      c,
      { comRedCalled: pComRedCalled, userRedCalled: pUserRedCalled }
    )
  }

  function memoCheck(state, step) {
    user(state, { user: 'me'} )
    assert.equal(
      compareCalled(false, false),
      true,
      `On others calls reducers are called if props and state remain the same - check ${step}`
    )
  }

  let newState = state

  user(newState)
  expect(
    compareCalled(false, false),
    // 'Reducers are not called if props not sent'
  ).toBe(true)

  user(newState, { user: 'me'} )
  expect(
    compareCalled(true, true),
    // 'On first call with props, reducers are called'
  ).toBe(true)
  memoCheck(newState, 1)

  newState = R.evolve({
    entities: {
      comments: {
        one: R.merge(R.__, {text: 'youhou'})
      }
    }
  }, state)

  user(newState, { user: 'me' } )
  expect(
    compareCalled(true, true),
    // 'if state of sub selector is changed, sub reducer and parent reducer are called'
  ).toBe(true)
  memoCheck(newState, 2)

  // update state slice of user selector
  newState = R.assocPath(
    ['entities', 'user', 'notme'],
    { name: 'gab' },
    newState
  )

  user(newState, { user: 'me'} )
  expect(
    compareCalled(false, true),
    // 'if only state of parent selector is changed, sub reducer is not called'
  ).toBe(true)
  memoCheck(newState, 3)
})
