import stampit from '@stamp/it'
import identity from 'ramda/src/identity'
import { createStructuredSelector } from 'reselect'
import PropTypes from '../prop_types'


export function makeIsCacheValid(comparator) {
  let lastResult
  return function isCacheValid(result) {
    if (comparator(result, lastResult)) return true
    lastResult = result
    return false
  }
}


function makeSubStateGetter(stamp) {
  const { objSelectors } = stamp.compose.deepProperties
  return Object.keys(objSelectors).length > 0
    ? createStructuredSelector(objSelectors)
    : identity
}


const SelectorDef = stampit()
  .deepProps({
    objSelectors: {},
  })
  .statics({
    PropTypes,
    propTypes(...args) {
      return this.statics({
        PropTypes: this.PropTypes.propTypes(...args)
      })
    },

    selectors(objSelectors) {
      const newStamp = this.deepProps({
        objSelectors,
      })

      return newStamp.methods({
        getSubState: makeSubStateGetter(newStamp)
      })
    },
  })
  .methods({
    getSubState: identity,
  })


export default SelectorDef
