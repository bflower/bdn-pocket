import stampit from '@stamp/it'


const DefState = stampit()
  .deepProps({
    defaultState: {}
  })
  .statics({
    defaultState(defaultState) {
      return this.deepProps({
        defaultState
      })
    },
  })

export default DefState
