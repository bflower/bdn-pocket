import stampit from '@stamp/it'
import { argOverProp } from '@stamp/arg-over-prop'

function constantize(str) {
  return str.toUpperCase().replace(/\s+/g, '_')
}

const ActionType = stampit(argOverProp('prefix', 'transformer'))
  .props({
    prefix: 'my-app',
    transformer: constantize
  })
  .methods({
    getType(type) {
      return `${this.prefix}/${this.transformer(type)}`
    }
  })

export default ActionType
