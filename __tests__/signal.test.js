import assert from 'assert'
import Action from '../src/action'
import Signal from '../src/signal'

test('Signal', () => {
  assert.equal(
    Signal,
    Action,
    "Signal is an action - it is only a semantic separation"
  )
})
