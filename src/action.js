import stampit from '@stamp/it'
import is from '@stamp/is'
import ActionType from './action_type'
import PropTypes, { Types } from './prop_types'

export { Types }


const Action = stampit(PropTypes)
  .conf({
    actionType: ActionType(),
  })
  .statics({
    CONST: '',
    prefix(prefix) {
      if (!is.isString(prefix)) {
        throw new ReferenceError(`expected prefix type is string - received ${typeof prefix}`)
      }
      return this.conf({
        actionType: ActionType({ prefix })
      })
    },
    def(name) {
      if (!is.isString(name)) {
        throw new ReferenceError(`expected name type is string - received ${typeof name}`)
      }
      const { actionType } = this.compose.configuration
      return this.statics({
        CONST: actionType.getType(name),
      })
    },
  })
  .props({
    type: '',
    payload: {},
  })
  .init((props, { stamp, instance }) => {
    const { CONST } = stamp.compose.staticProperties
    if (CONST.length === 0) {
      throw new Error('action has not name definition - use Action.def to init it')
    }
    instance.type = CONST
    instance.payload = props
  })

// const a = Action.def('a').propTypes('a')
// console.log(a({a: 'a'}))

export default Action
