import assert from 'assert'
import R from 'ramda'
import { Messenger } from '../src/messenger'
import Action from '../src/action'

import { checkMessages } from './lib'

test('Messenger - a link between action and reducer', () => {
  const actionKey = 'actionKey'
  const myAction = Action.def('hello')
  const reducer = (state, action) => 'reducer'
  const msgr = Messenger
    .add({
      key: actionKey,
      action: myAction,
      reducer,
    })
    .create()

 expect(
    R.has(actionKey, msgr)
    // 'Messenger has actionKey in its props'
  ).toBe(true)

  expect(
    msgr[actionKey] === myAction
    // 'Messenger has action definition'
  ).toBe(true)

  expect(
    R.has('reducers', msgr)
    // 'Messenger has reducers props'
  ).toBe(true)

  expect(
    R.has(myAction.CONST, msgr.reducers)
    // 'You can retreive reducer linked to an action definition'
  ).toBe(true)
  expect(
    msgr.reducers[myAction.CONST]() === reducer()
    // 'function linked to action definition is the defined reducer'
  ).toBe(true)
})

test('Messenger#add', () => {
  const reducer = (state, action) => state
  const msgr = Messenger
    .add({
      key: 'a',
      action: Action.def('a'),
      reducer,
    })
    .add({
      key: 'b',
      action: Action.def('b'),
      reducer,
    })
    .add({
      key: 'c',
      action: Action.def('c'),
      reducer,
    })
    .create()

  checkMessages(msgr, ['a', 'b', 'c'])
})

test('Messenger#add checks', () => {
  const reducer = (state, action) => state
  const msgr = Messenger
    .add({
      key: 'a',
      action: Action.def('a'),
      reducer,
    })

  assert.throws(
    () => (
      msgr.add({

      })
    ),
    Error,
    'key is mandatory'
  )

  assert.throws(
    () => (
      msgr.add({
        key: 'hello'
      })
    ),
    Error,
    'action must be defined and have a CONST prop'
  )
  assert.throws(
    () => (
      msgr.add({
        key: 'hello',
        action: Action.def('b')
      })
    ),
    Error,
    'reducer must be a function'
  )

  assert.throws(
    () => (
      msgr.add({
        key: 'a',
        action: Action.def('b'),
        reducer
      })
    ),
    Error,
    'you cannot add duplicate keys'
  )
})
