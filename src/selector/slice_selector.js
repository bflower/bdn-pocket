import stampit from '@stamp/it'

import SelectorDef, { makeIsCacheValid } from './selector_def'

const SliceSelector = stampit(SelectorDef)
  .init(({ reducer }, { stamp, instance }) => {
    const { PropTypes } = stamp.compose.staticProperties
    const {
      getSubState,
    } = instance
    const isSubStateCacheValid = makeIsCacheValid((r, l) => r === l)

    let lastPartialReducer

    /**
     * propsReducer generate a partial reducer function
     * that is memoized (lastPartialReducer)
     * if props is sent, propsReducer execute the partial reducer
     * else it return the partial reducer
     */
    return function propsReducer(state, props) {
      const subStates = getSubState(state)
      if (!isSubStateCacheValid(subStates)) {
        lastPartialReducer = props => {
          PropTypes(props)
          return reducer(subStates, props)
        }
      }

      return arguments.length > 1 ?
        lastPartialReducer(props) :
        lastPartialReducer
    }
  })

export default SliceSelector
