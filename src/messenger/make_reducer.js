import has from 'ramda/src/has'

export default function makeReducer(messenger) {
  const { defaultState, reducers } = messenger
  const reducer = (state = defaultState, action) => (
    has(action.type, reducers) ?
      reducers[action.type](state, action) :
      state
  )
  return reducer
}
