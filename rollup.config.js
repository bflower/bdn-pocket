import nodeResolve from 'rollup-plugin-node-resolve'
import babel from 'rollup-plugin-babel'
import replace from 'rollup-plugin-replace'
import uglify from 'rollup-plugin-uglify'
import commonjs from 'rollup-plugin-commonjs'

const env = process.env.NODE_ENV
const file = env === 'production' ?
  'dist/bdn-pocket.min.js' :
  'dist/bdn-pocket.js'

const config = {
  input: 'src/index.js',
  output: {
    name: 'bdn-pocket',
    format: 'umd',
    file,
  },
  plugins: [
    nodeResolve({
      jsnext: true,
    }),
    babel({
      babelrc: false,
      exclude: 'node_modules/**',
      plugins: ['external-helpers'],
      env: {
        cjs: {
          presets: [["env", { loose: true }]]
        },
        es: {
          presets: [["env", { loose: true, modules: false }]]
        }
      }
    }),
    replace({
      'process.env.NODE_ENV': JSON.stringify(env),
    }),
    commonjs({
      namedExports: {
        'node_modules/@stamp/it/index.js': ['default'],
        'node_modules/@stamp/arg-over-prop/index.js': ['argOverProp'],
      },
    })
  ]
}

if (env === 'production') {
  config.plugins.push(
    uglify({
      compress: {
        pure_getters: true,
        unsafe: true,
        unsafe_comps: true,
        warnings: false
      }
    })
  )
}

export default config
