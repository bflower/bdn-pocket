import assert from 'assert'
import PropTypes, { Types } from '../src/prop_types'

const { string, array, object } = Types

test('PropTypes', () => {
  const c = PropTypes.propTypes({
    a: string,
    b: string,
  })

  assert.doesNotThrow(
    () => c({a: 'a', b: 'b'}),
    'validate an object according to its prop types - only string'
  )

  assert.throws(
    () => c({a: 10, b: 'b'}),
    Error,
    'validate an object according to its prop types - bad type'
  )

  assert.throws(
    () => c({a: 'a'}),
    Error,
    'validate an object according to its prop types - missing prop'
  )
})

test('PropTypes shortcut', () => {
  const c = PropTypes.propTypes('a', 'b')
  assert.doesNotThrow(
    () => c({a: 'a', b: 'b'}),
    'validate an object with string type and required value'
  )

  assert.doesNotThrow(
    () => c({a: 10, b: 'b'}),
    'validate an object with string type and required value - bad type'
  )

  assert.throws(
    () => c({a: 'a'}),
    Error,
    'validate an object with string type and required value - missing prop'
  )
})


test('PropTypes with type definition', () => {
  const c = PropTypes.propTypes({
    a: string,
    b: string({ required: false, allowNull: true }),
  })

  assert.doesNotThrow(
    () => c({a: 'a', b: 'b'}),
    'validate an object according to its prop types - only string'
  )

  assert.doesNotThrow(
    () => c({ a: 'a' }),
    'validate an object according to its prop types - allowed missing prop'
  )
  assert.doesNotThrow(
    () => c({ a: 'a', b: null }),
    'validate an object according to its prop types - allowed null prop'
  )

  assert.throws(
    () => c({ b: 'b'}),
    Error,
    'validate an object according to its prop types - missing prop'
  )
})

test('PropTypes.hasPropTypes', () => {
  const c = PropTypes.propTypes({
    a: string,
    b: string({ required: false, allowNull: true }),
  })

  expect(
    c.hasPropTypes(),
    // 'return true if prop types defined'
  ).toBe(true)

  expect(
    PropTypes.hasPropTypes(),
    // 'return false if not proptypes defined'
  ).toBe(false)
})

test('PropType array', () => {
  const c = PropTypes.propTypes({
    a: array,
    b: array({ required: false, allowNull: true }),
  })

  assert.throws(
    () => c({ a: 1, b: ['hello']}),
    Error,
    'validate required array prop - bad format'
  )

  assert.throws(
    () => c({ b: ['hello']}),
    Error,
    'validate required array prop - missing prop'
  )

  assert.doesNotThrow(
    () => c({ a: ['hello']}),
    'validate required array prop - not required prop'
  )
})

test('PropType array vs object', () => {
  const cObj = PropTypes.propTypes({
    a: object,
  })
  const cArray = PropTypes.propTypes({
    a: array,
  })

  assert.throws(
    () => cObj({ a: ['hello'] }),
    Error,
    'validate object prop - array throws error'
  )

  assert.doesNotThrow(
    () => cObj({ a: {} }),
    'validate object - correct format'
  )

  assert.throws(
    () => cArray({ a: {} }),
    Error,
    'validate array prop - object throws error'
  )

  assert.doesNotThrow(
    () => cArray({ a: [] }),
    'validate array prop - correct format'
  )
})
