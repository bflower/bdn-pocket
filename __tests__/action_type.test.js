import ActionType from '../src/action_type'
import assert from 'assert'

test('ActionType', () => {
  assert.equal(
    ActionType().getType('hello you'),
    'my-app/HELLO_YOU',
    'by default actiontype is prefixed by my-app and type is constantize'
  )
  assert.equal(
    ActionType({
      prefix: 'coucou',
      transformer: x => x,
    }).getType('hello you'),
    'coucou/hello you',
    'you can override prefix & transformer type function'
  )
})
