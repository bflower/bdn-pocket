import has from 'ramda/src/has'
import path from 'ramda/src/path'
import assocPath from 'ramda/src/assocPath'
import dissocPath from 'ramda/src/dissocPath'

import makeReducer from './make_reducer'

function ensureArray(v) {
  return Array.isArray(v) ? v : [v]
}

const DELETE_KEY = {
  del: 'my key'
}

function makePathReducer(messenger, pathGetter) {
  const { defaultState } = messenger
  const subReducer = makeReducer(messenger)
  const { reducers } = messenger
  const reducer = (state = defaultState, action) => {
    if (!has(action.type, reducers)) return state
    const pathArr = ensureArray(pathGetter(action))
    const subState = path(pathArr, state)
    const newSubState = subReducer(subState, action)

    return newSubState === DELETE_KEY ?
      dissocPath(pathArr, state) :
      assocPath(pathArr, newSubState, state)
  }
  return reducer
}

makePathReducer.DELETE_KEY = DELETE_KEY

export default makePathReducer
