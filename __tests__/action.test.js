import Action, { Types } from '../src/action'
import ActionType from '../src/action_type'
import assert from 'assert'

const { string } = Types

test('Action definition', () => {
  const a = Action.def('my action')
  assert.equal(
    a.CONST,
    'my-app/MY_ACTION',
    'generate an action creator with a constant defined by def function'
  )

  // console.log(a({ propA: 'a' }))
})

test('Action redefinition', () => {
  const myActionType = ActionType({
    prefix: 'coucou',
    transformer: x => x,
  })
  const MyActionDef = Action.conf({ actionType: myActionType })
  const b = MyActionDef.def('my action')
  assert.equal(
    b.CONST,
    'coucou/my action',
    'you can override default Action behaviour by defining a new ActionDefiniton (ActionType)'
  )

  const MyOtherActionDef = Action.prefix('hello')
  const c = MyOtherActionDef.def('my other action')
  assert.equal(
    c.CONST,
    'hello/MY_OTHER_ACTION',
    'you can override default Action behaviour by changing prefix'
  )

  const v = Action.def('initial action')
  assert.equal(
    v.CONST,
    'my-app/INITIAL_ACTION',
    'initial action creator keep always the same configuration'
  )
})


test('Action#create action', () => {
  // const func = () => {
  //   Action({ a: 'a' })
  // }
  // expect(() => {
  //   func()
  // }).toThrow();

  assert.throws(
    () => Action({ a: 'a' }),
    Error,
    'You must define a name of an action creator'
  )

  const myAction = Action.def('action name')
  const payload = {a: 'a', b: 'b' }
  const a = myAction(payload)

  assert.equal(
    a.type,
    myAction.CONST,
    'create an action with type equals to CONST value'
  )

  assert.deepEqual(
    a.payload,
    payload,
    'create an action with payload equals to props send as first argument'
  )
})

test('Action#propTypes', () => {
  const myAction = Action
    .def('action name')
    .propTypes('a', 'b')
  const myAction2 = Action
    .def('action name 2')
    .propTypes({
      a: string,
      b: string,
    })
  const actions = [myAction, myAction2]

  actions.forEach(a => {
    assert.doesNotThrow(           
      () => a({ a: 'a', b: 'b' }),
      'you can ensure props validation - props ok'
    )

    assert.throws(
      () => a({ a: 'a' }),
      Error,
      'you can ensure props validation - missing prop'
    )
  })

  assert.throws(
    () => myAction2({ a: 'a', b: 10 }),
    Error,
    'you can ensure props validation - bad type prop'
  )
})

